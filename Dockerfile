FROM python:3.8.6-slim-buster

ENV DEBIAN_FRONTEND=noninteractive 

# install system dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get install -y \
        build-essential \
        python3-pip \
        libsm6 \
        libxext6 \
        libxrender-dev \
        python3-opencv \
	wget \
	curl \
	vim \
    && rm -rf /var/lib/apt/lists/*

ENV PATH /usr/bin/pip:$PATH
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

WORKDIR /home
ENV PYTHONPATH=/usr/bin/python3.8

# get python requirements (cachable)
RUN pip3 install --no-cache-dir pipenv

ADD robotec /home/robotec

WORKDIR /home

COPY ./Pipfile .
COPY ./Pipfile.lock .

RUN pipenv install --system --deploy
WORKDIR /home/robotec
