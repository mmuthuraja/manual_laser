FROM python:3.8.6-slim-buster

ENV DEBIAN_FRONTEND=noninteractive 

# install system dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get install -y \
        build-essential \
        python3-pip \
        libsm6 \
        libxext6 \
        libxrender-dev \
        python3-opencv \
	    wget \
	    curl \
	    vim \
    && rm -rf /var/lib/apt/lists/*

ENV PATH /usr/bin/pip:$PATH
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

WORKDIR /home
ENV PYTHONPATH=/usr/bin/python3.8

# get python requirements (cachable)
RUN pip3 install --no-cache-dir pipenv

WORKDIR /home

COPY ./Pipfile .
COPY ./Pipfile.lock .

RUN pipenv install --system --deploy --dev

RUN jupyter notebook --generate-config \
    && printf '%s\n' "c.NotebookApp.ip = '0.0.0.0'" "c.NotebookApp.token = ''" \
    "c.NotebookApp.allow_origin = '*'" "c.NotebookApp.disable_check_xsrf = True" > /root/.jupyter/jupyter_notebook_config.py