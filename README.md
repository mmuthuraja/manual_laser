# RoBoTEC Application

The main web application providing the back-bone of the RoBoTEC platform.

## Installation

1. Clone the repository

    `git clone http://gitlab.robotec-ptc.com/robotec/robotec-application.git`


2. Go to your project folder.

    `cd robotec-application`

3. Build the Docker image

    `docker build -t robocut_application:manual_laser -f dev.Dockerfile . --no-cache=true` <br>

   Add "sudo" before the command when you encounter permission issues.

4. Run the Container

    `cd ..`

    `docker run -it --rm -v $PWD/robotec-application:/home/ -p 5000:5000 --user "$(id -u):$(id -g)"  --name  rb_appl_cont  robocut_application:manual_laser bash`
    
   when something like below shows up, meaning you are now in a Docker terminal running the image in a container.

   `groups: cannot find name for group ID 1000`<br>
   `I have no name!@c7f427f7a690:/home$ `


## Running

Inside the docker terminal, 

`cd robotec/` <br>

Run Migration once

`python manage.py makemigrations` <br>
`python manage.py migrate` <br>

To run the server

`python manage.py runserver 0:5000` <br>

For Manual laser, Open the browser and go to http://localhost:5000/manual-laser

## Test

Inside the docker terminal:

`pytest` 

`pytest robotec/` 

`flake8`

## Using Jupyter Notebook for quick development
For quick development the dev.Dockerfile supports jupyter notebook now. To run the application inside the docker you need to start docker image in a similar way as defined above (permission must be given).

1. Outside of the robocut-application:
`docker run -it --rm --mount type=bind,src=$(pwd),dst=/home/ -p 8888:8888 --name rb_appl_cont robocut_application:manual_laser bash`

2. Inside the container start the jupyter notebook:
`jupyter notebook --allow-root`

Afterwards, you should be able to acces the jupyter notebook in your local web browser onto the http://localhost:8888.


## Fake Camera Server for testing purposes

Change the `CAMERA_GATEWAY_IP = "helios" ` in `robotec/settings.py` to `CAMERA_GATEWAY_IP = "localhost" `

<br>
Open a new terminal and inside: <br>

Connect to the container, that is running the django-application:
`docker exec -it rb_appl_cont bash` <br>

Run the fake camera server:
`cd robotec/manual_laser/utilities` <br>
`python fake_camera_server.py` <br>



# Arc42 Architecture Documentation
[XWiki - Arc42](http://xwiki.robotec-ptc.com/bin/view/Projects/Architecture%20documentation%20RoBoTEC%20Application/)

# User Documentation
[XWiki - User Doc](http://xwiki.robotec-ptc.com/bin/view/Projects/The%20Manual%20Laser%20Documentation/)
