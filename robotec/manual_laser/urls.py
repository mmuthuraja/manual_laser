"""URLs."""

from django.urls import path

from . import views

urlpatterns = [
    # ex: /manual_laser
    path('', views.manual_laser, name='manual_laser'),
    # ex: manual_laser/get_image
    path('get-image', views.get_image, name='get_image'),
    # ex: manual_laser/laser_cut
    path('laser-cut', views.laser_cut, name='laser_cut'),
]
