"""Utility Functions."""

from .models import CutlineModel, StereoImagesModel
from .laser_calculations import CutLineGeneration
import datetime
import uuid
import cv2
import os
import base64
import numpy as np

from django.core.exceptions import ObjectDoesNotExist


def save_stereo_images(stereo_image_dict):
    """Save stereo images to database model.

    Args:
        stereo_image_dict: dict with key 'left_image' and 'right_image'

    Returns:
         generated stereo id

    """
    stereo_image_model = StereoImagesModel()
    stereo_image_model.left_image = stereo_image_dict['left_image']
    stereo_image_model.right_image = stereo_image_dict['right_image']
    stereo_image_model.stereo_id = str(uuid.uuid4())
    stereo_image_model.save()

    return stereo_image_model.stereo_id


def save_cutline_and_metadata(json_data):
    """Save cutline info and metadata info.

    Args:
        json_data: dict with cutlines and metadata

    """
    cutlinemodel = CutlineModel()
    cutlinemodel.image_data = json_data['imageData']
    cutlinemodel.image_id = json_data['imageId']
    cutlinemodel.cut_line = json_data['cutLine']
    cutlinemodel.operator_name = json_data['operatorName']
    cutlinemodel.plant_species = json_data['plantSpecies']
    cutlinemodel.clone_id = json_data['cloneId']
    cutlinemodel.customer_info = json_data['customerInfo']
    cutlinemodel.power_value = json_data['powerValue']
    cutlinemodel.time_stamp = datetime.datetime.now().astimezone() \
        .replace(microsecond=0).isoformat()
    cutlinemodel.save()


def convert_base64_image_string_to_numpy_array(base64_string):
    """Convert base64 string image into a numpy array.

    Args:
        image_string: Base64 image of class 'str'

    Returns:
        image_array_in_numpy: image array in numpy format

    """
    image_in_bytes = base64_string.encode('utf-8')
    image_in_binary = base64.decodebytes(image_in_bytes)
    image_array = np.frombuffer(image_in_binary, np.uint8)
    image_array_in_numpy = cv2.imdecode(image_array, -1)
    return image_array_in_numpy


def un_mirror_image(image):
    """Un mirror the image."""
    un_mirrored_image = cv2.flip(image, 1)
    return un_mirrored_image


def rectify_image(image):
    """Rectify the image."""
    cutline_obj = CutLineGeneration()
    rectified_image = cv2.remap(image,
                                cutline_obj.rectified_map1_left_image,
                                cutline_obj.rectified_map2_left_image,
                                cv2.INTER_LINEAR)
    return rectified_image


def rotate_base64_image_string(image_string,
                               direction=cv2.ROTATE_90_CLOCKWISE):
    """Rotate base64 image string.

    Args:
        image_string: Base64 image of class 'str'
        direction: Direction of rotation

    Returns:
        image_string_rotated: Rotated Base64 image in of class 'str'

    """
    # Applying rotation anti-clockwise 90
    image_array_in_numpy = \
        convert_base64_image_string_to_numpy_array(image_string)

    un_mirrored_image = un_mirror_image(image_array_in_numpy)
    rectified_image = rectify_image(un_mirrored_image)

    image_rotated = cv2.rotate(rectified_image,
                               direction)

    # Converting rotated into base64 string
    _, buffer_img = cv2.imencode('.png', image_rotated)
    image_rotated_base64 = base64.b64encode(buffer_img).decode()

    return image_rotated_base64


def load_image_from_test_images_folder():
    """Load images from test images folder."""
    root_folder = os.path.abspath(".").split("/")[-1]
    if "home" == root_folder or \
            "robotec-application" == root_folder:
        data_path = os.path.abspath("robotec")
    else:
        data_path = os.path.abspath(".")
    left_img_path = "manual_laser/test_images/img/Left_Raw.png"
    right_img_path = "manual_laser/test_images/img/Right_Raw.png"
    left_stereo_image = cv2.imread(os.path.join(data_path,
                                                left_img_path))
    right_stereo_image = cv2.imread(os.path.join(data_path,
                                                 right_img_path))

    return left_stereo_image, right_stereo_image


def obtain_stereo_images(image_id):
    """Load stereo images from the database using image_id.

    Args:
        image_id

    Returns:
        left and right stereo image

    """
    stereo_images = StereoImagesModel.objects.get(stereo_id=image_id)
    left_stereo_image = \
        convert_base64_image_string_to_numpy_array(
            stereo_images.left_image)
    disparity_map = np.frombuffer(base64.b64decode(stereo_images.right_image),
                                  np.float32)
    disparity_map = disparity_map.reshape(1536, 2048)

    # right_stereo_image = \
    #     convert_base64_image_string_to_numpy_array(
    #         stereo_images.right_image)

    left_stereo_image = cv2.cvtColor(left_stereo_image,
                                     cv2.COLOR_RGB2BGR)
    # right_stereo_image = cv2.cvtColor(right_stereo_image,
    #                                   cv2.COLOR_RGB2BGR)

    return left_stereo_image, disparity_map


def rotate_by_ninety_degree(points, image_height):
    """Rotate the points by 90 degree."""
    image_height -= 1

    points_new = []
    for point in points:
        x_new = point[1]
        y_new = image_height - point[0]
        points_new.append([x_new, y_new])
    return points_new


def is_valid_data(json_data):
    """Check for valid data."""
    if len(json_data['cutLine']):
        expected_keys = ['imageData', 'imageId', 'cutLine',
                         'operatorName', 'plantSpecies', 'cloneId',
                         'customerInfo', 'powerValue']

        iterator = 0
        for key in json_data:
            for value in expected_keys:
                if key == value:
                    iterator += 1
                    break
        if iterator == len(expected_keys):
            return True
    return False


def is_valid_image_id(image_id, queryset):
    """Check image id exists."""
    if len(queryset):
        try:
            queryset.get(stereo_id=image_id)
            return True
        except ObjectDoesNotExist:
            return False
    return False
