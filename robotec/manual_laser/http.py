"""Encapsulated low-level communication code."""

import http.client
import json
from django.http import HttpResponse
from django.conf import settings
import requests
import numpy as np
import base64
import requests
from io import BytesIO
from PIL import Image, ImageDraw


def get_stereo_images():
    """Client for getting image from camera gateway."""
    url_join_items = [
        settings.CAMERA_GATEWAY_IP + ':' + str(settings.CAMERA_GATEWAY_PORT),
        "get-images"]
    camera_image_url = "http://{}".format("/".join(url_join_items))
    response_dict = requests.get(camera_image_url).json()
    stereo_image_dict = {
        "left_image": response_dict['left_image']['data'],
        "right_image": response_dict['right_image']['data']
    }
    return stereo_image_dict


def post_data_to_laser_gateway(json_data):
    """Post data to the laser gateway."""
    headers = {'Content-type': 'application/json'}
    laser_gateway_connection = http.client.HTTPConnection(
        settings.LASER_GATEWAY_IP, settings.LASER_GATEWAY_PORT)

    laser_gateway_connection.request('POST', '/laser-cut',
                                     json.dumps(json_data),
                                     headers)
    response = laser_gateway_connection.getresponse()
    response_dict = json.loads(response.read())
    if response_dict['job_status'] == "accepted":
        return HttpResponse("Finished")
    else:
        return HttpResponse("Laser Busy")


def encode_into_base64_img_string(np_image_array, cut_lines=[]):
    if np_image_array.size == 0:
        return ""
    pil_img = Image.fromarray(np_image_array)

    if cut_lines:
        draw = ImageDraw.Draw(pil_img)
        for i in range(len(cut_lines)-1):
            point_1 = cut_lines[i]
            point_2 = cut_lines[i+1]
            draw.line((point_1[0], point_1[1], point_2[0], point_2[1]),
                      fill=128)

    # pil_img = scale_image_with_aspect_ratio(pil_img,
    #                                         max(pil_img.size[0],
    #                                             pil_img.size[1]))

    if pil_img.mode != 'RGB':
        pil_img = pil_img.convert("RGB")
    buff = BytesIO()
    pil_img.save(buff, format="PNG")
    return base64.b64encode(buff.getvalue()).decode("utf-8")


def trigger_camera(camera_ip, camera_port):
    headers = {'Content-type': 'application/json'}
    camera_connection = http.client.HTTPConnection(camera_ip, camera_port)
    camera_connection.request('PUT', "/image", "", headers)
    response = camera_connection.getresponse()
    if response.status == 200:
        return True
    return False



def get_encoded_image(image_dict, cut_lines=[]):
    encoded_img = ""
    img_base64_str = image_dict['data']
    if img_base64_str == "":
        return encoded_img
    image_array = np.frombuffer(base64.b64decode(img_base64_str),
                                np.uint8)
    image = image_array.reshape(
        image_dict['size']['rows'],
        image_dict['size']['cols'],
        image_dict['size']['channels']
    )
    image = image[:, :, ::-1]  # bgr to rgb
    if cut_lines:
        encoded_img = encode_into_base64_img_string(image, cut_lines)

    encoded_img = encode_into_base64_img_string(image)
    return encoded_img

def scale_image_with_aspect_ratio(img, desired_size):

    old_size = img.size
    if max(old_size) == 0:
        return img
    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])
    img = img.resize(new_size, Image.ANTIALIAS)
    new_img = Image.new(img.mode, (desired_size, desired_size))
    new_img.paste(img, ((desired_size-new_size[0])//2,
                        (desired_size-new_size[1])//2))

    return new_img


def get_images_and_maps_from_camera_daemon(camera_ip, camera_port):
    """Client for getting images and maps from camera daemon."""
    images_and_maps = dict()
    print("here")
    # try:
    url_join_items = [
        camera_ip + ':' + str(camera_port),
        ""]
    camera_image_url = "http://{}".format("/".join(url_join_items))
    is_triggered = trigger_camera(camera_ip, camera_port)
    print("is_triggered:", is_triggered)
    if not is_triggered:
        return images_and_maps
    response = requests.get(camera_image_url)

    if response.status_code == 200:
        response_dict = response.json()
        encoded_img = ""
        encoded_disparity = ""
        encoded_depth = ""

        if response_dict['image']['data'] != "":
            encoded_img = get_encoded_image(response_dict['image'])

        if 'disparity_map' in response_dict.keys() and \
                response_dict['disparity_map']['data'] != "":
            encoded_disparity = response_dict['disparity_map']['data']

            # disp_array = np.frombuffer(base64.b64decode(disp_base64_str),
            #                            np.float32)
            # disparity_map = disp_array.reshape(
            #     response_dict['disparity_map']['size']["rows"],
            #     response_dict['disparity_map']['size']["cols"]
            #     ).astype(np.uint8)
            # encoded_disparity = encode_into_base64_img_string(
            #     disparity_map)

        if 'depth_map' in response_dict.keys() and \
                response_dict['depth_map']['data'] != "":
            depth_base64_str = response_dict['depth_map']['data']
            depth_array = np.frombuffer(base64.b64decode(depth_base64_str),
                                        np.float32)
            depth_map = depth_array.reshape(
                response_dict['depth_map']['size']["rows"],
                response_dict['depth_map']['size']["cols"],
                response_dict['depth_map']['size']["channels"]
            ).astype(np.uint8)
            encoded_depth = encode_into_base64_img_string(depth_map)

        images_and_maps['encoded_img'] = encoded_img
        images_and_maps['encoded_disparity'] = encoded_disparity
        images_and_maps['encoded_depth'] = encoded_depth

    return images_and_maps

    # except OSError:
    #     return images_and_maps
