"""Create your models here."""

from django.db import models


# Create your models here
class CutlineModel(models.Model):
    """Cut Line model class."""

    image_id = models.CharField(max_length=200, default='12345')
    image_data = models.CharField(max_length=15000000000, default='12345')
    cut_line = models.CharField(max_length=1500000000, default='12345')
    operator_name = models.CharField(max_length=200, default='12345')
    plant_species = models.CharField(max_length=200, default='12345')
    clone_id = models.CharField(max_length=200, default='12345')
    customer_info = models.CharField(max_length=200, default='12345')
    time_stamp = models.CharField(max_length=200, default='12345')
    power_value = models.CharField(max_length=200, default='12345')


class StereoImagesModel(models.Model):
    """StereoImagesModel for storing images."""

    stereo_id = models.CharField(max_length=200, default='12345')
    left_image = models.CharField(max_length=15000000000, blank=True,
                                  default='12345')
    right_image = models.CharField(max_length=15000000000, blank=True,
                                   default='12345')
