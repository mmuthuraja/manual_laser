"""Register your models here."""

from django.contrib import admin
from .models import CutlineModel, StereoImagesModel

admin.site.register(CutlineModel)
admin.site.register(StereoImagesModel)
