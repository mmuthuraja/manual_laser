"""Manual laser app config."""

from django.apps import AppConfig


class ManualLaserConfig(AppConfig):
    """ManualLaserConfig class."""

    name = 'manual_laser'
