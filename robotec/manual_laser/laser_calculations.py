"""Operations."""

import os
import numpy as np
import yaml
import cv2
import math
from scipy.spatial.transform import Rotation
from django.conf import settings


class CutLineGeneration:
    """3D_Cut_line."""

    def __init__(self):
        """Cut line generation constructor."""
        self.image_height = 1536
        self.image_width = 2048
        self.input_image_size = (self.image_width, self.image_height)
        self.calibration_dict = CutLineGeneration.read_calibration_matrices()

        self.left_upper_intrinsic = np.array(
            self.calibration_dict['LeftUpperIntrinsic'],
            dtype=np.float32)

        self.left_upper_distortion = np.array(
            self.calibration_dict['LeftUpperDistortion'],
            dtype=np.float32)

        self.left_upper_rotation = np.array(
            self.calibration_dict['LeftUpperRotation'],
            dtype=np.float32)

        self.left_upper_projection = np.array(
            self.calibration_dict['LeftUpperProjection'],
            dtype=np.float32)

        self.left_lower_intrinsic = np.array(
            self.calibration_dict['LeftLowerIntrinsic'],
            dtype=np.float32)

        self.left_lower_distortion = np.array(
            self.calibration_dict['LeftLowerDistortion'],
            dtype=np.float32)

        self.left_lower_rotation = np.array(
            self.calibration_dict['LeftLowerRotation'],
            dtype=np.float32)

        self.left_lower_projection = np.array(
            self.calibration_dict['LeftLowerProjection'],
            dtype=np.float32)

        self.rectified_map1_left_image, self.rectified_map2_left_image = \
            cv2.initUndistortRectifyMap(self.left_upper_intrinsic,
                                        self.left_upper_distortion,
                                        self.left_upper_rotation,
                                        self.left_upper_projection,
                                        self.input_image_size,
                                        cv2.CV_32FC1)

        self.rectified_map1_right_image, self.rectified_map2_right_image = \
            cv2.initUndistortRectifyMap(self.left_lower_intrinsic,
                                        self.left_lower_distortion,
                                        self.left_lower_rotation,
                                        self.left_lower_projection,
                                        self.input_image_size,
                                        cv2.CV_32FC1)

        self.q_matrix = np.array(
            self.calibration_dict['cvInputQ'],
            dtype=np.float32)

        self.projection_matrix_to_laser = np.array(
            self.calibration_dict['LaserProjMatrix'],
            dtype=np.float32)

        self.origin_translation_vector_before = np.array(
            self.calibration_dict['OriginTranslationVectorBefore'],
            dtype=np.float32)

        self.origin_translation_vector_after = np.array(
            self.calibration_dict['OriginTranslationVectorAfter'],
            dtype=np.float32)

        self.origin_euler_angles = np.array(
            self.calibration_dict['OriginEulerAngles'],
            dtype=np.float32)

    @staticmethod
    def point_distance(point1, point2):
        """Calculate the distance of two points.

        Args:
            point1: [x,y]
            point2: [x,y]
        Returns:
            distance: [dx,dy]

        """
        return math.hypot(point2[0] - point1[0], point2[1] - point1[1])

    @staticmethod
    def read_calibration_matrices():
        """Load calibration matrices from YAML file."""
        root_folder = os.path.abspath(".").split("/")[-1]
        if "home" == root_folder or "robotec-application" == root_folder:
            data_path = os.path.abspath("robotec")
        else:
            data_path = os.path.abspath(".")

        if settings.MACHINE == 'manual_laser':
            config_file = 'camera_matrices_manual_laser.yml'
        else:
            config_file = 'camera_matrices_rbc_machine.yml'

        data_path = os.path.join(data_path, config_file)
        print('Reading camera_matrices from:', data_path)
        with open(data_path, 'r') as file:
            calibration_dict = yaml.load(file, Loader=yaml.FullLoader)
        return calibration_dict

    @staticmethod
    def compute_disparity_map(img_left, img_right):
        """Opencv stereo block matching for disparity calculation.

        Args:
            img_left: np.array - uint8
            img_right: np.array - uint8

        Returns:
            disparity: np.array - float32

        """
        # TODO: why exactly this magic numbers and should we shift it to
        #       to a more general place?
        # -> we have to document these values first (another task)
        sbm = cv2.StereoBM_create()
        sbm.setMinDisparity(0)
        sbm.setNumDisparities(480)
        sbm.setBlockSize(21)
        sbm.setSmallerBlockSize(0)
        sbm.setPreFilterType(0)
        sbm.setPreFilterCap(31)
        sbm.setPreFilterSize(31)
        sbm.setUniquenessRatio(0)
        sbm.setTextureThreshold(0)
        sbm.setSpeckleWindowSize(400)
        sbm.setSpeckleRange(10)
        sbm.setDisp12MaxDiff(20)

        gray_left = cv2.cvtColor(img_left, cv2.COLOR_BGR2GRAY)
        gray_right = cv2.cvtColor(img_right, cv2.COLOR_BGR2GRAY)

        disparity = sbm.compute(gray_left,
                                gray_right).astype(np.float32) / 16.0
        return disparity

    @staticmethod
    def calculate_3D_cut_point(point_2D, disparity_map, q_matrix):
        """Transform 2D image coordinates into 3D using disparity map.

        Args:
            point_2D: list of [x,y,z]
            disparity_map: np.array - float32
            cvInputQ: np.array - float64

        Returns:
            points_3D: list of [x,y,z]

        """
        points_3D = []
        disparity_value = disparity_map[point_2D[1], point_2D[0]]

        invalid_disparity_value = -1
        magic_threshold_for_useful_disparity_value = 101

        if (disparity_value <= invalid_disparity_value):
            disparity_value = \
                CutLineGeneration.find_disparity(point_2D, disparity_map)

        if (disparity_value >= magic_threshold_for_useful_disparity_value):
            point_2D_and_disparity = np.array([[[point_2D[0],
                                                 point_2D[1],
                                                 disparity_value]]],
                                              dtype=np.float32)

            # computes the actual 3D point from the [point_2D, disparity_value]
            point_3D = cv2.perspectiveTransform(point_2D_and_disparity,
                                                q_matrix)

            # matching the world coordinates by changing the x- and y-axis
            point_3D[0][0][0] *= -1
            point_3D[0][0][1] *= -1
            points_3D.append(point_3D[0][0].tolist())

        return points_3D

    @staticmethod
    def calculate_3D_Cut_curve(cut_curve_2D, disparity_map, q_matrix):
        """Calculate 3D curve given 2D curve onto the image spece.

        Args:
            cutCurve_2D: list of [x,y]
            disparitymap: np.array - float32
            q_matrix: np.array - float64

        Returns:
            cutcurve_3D: list of [x,y,z]

        """
        cut_curve_3D = []
        for point_2D in cut_curve_2D:
            point_3D = CutLineGeneration.calculate_3D_cut_point(point_2D,
                                                                disparity_map,
                                                                q_matrix)
            if len(point_3D) != 0:
                cut_curve_3D = cut_curve_3D + point_3D

        return cut_curve_3D

    def get_median_focal_plane_adjustment(self, points_3D):
        """Calculate the focal adjustment factor given camera parameters.

        Args:
            points_3D: list of [x,y,z]

        Returns:
            median_focal_plane_adjustment: float64

        """
        transformation_matrix = self.get_camera_matrix()

        focuses = []
        for point_3D in points_3D:
            point_3D = np.concatenate([point_3D, [1]])
            point_3D = np.matmul(transformation_matrix, point_3D)
            focuses.append(point_3D[2])

        focuses.sort()
        median_focal_plane = 0

        if len(focuses) > 0:
            median_focal_plane = focuses[len(focuses) // 2]
        median_focal_plane_adjustment = -median_focal_plane * 1000

        return median_focal_plane_adjustment

    @staticmethod
    def compensate_focus(cutline_3D, focalplane_adjustment):
        """Compensate focus of the 3D cut line using focalplane_adjustment.

        Args:
            cutline_3D: list of [x,y,z]
            focalplane_adjustment: float64

        Returns:
            points_with_focus_compensation: list of [x,y,z]

        """
        # TODO: why exactly this magic numbers and should we shift it to
        #       to a more general place?
        # -> this place is fine, but solve the first question
        laser_workspace_magic_y_for_factor = 0.0032
        laser_workspace_magic_x_for_factor = 0.0027
        laser_workspace_magic_x_for_offset = 10000
        laser_workspace_magic_y_for_offset = 2

        x_factor = 1 - (laser_workspace_magic_x_for_factor *
                        focalplane_adjustment)
        y_factor = 1 - (laser_workspace_magic_y_for_factor *
                        focalplane_adjustment)

        x_offset = -focalplane_adjustment / laser_workspace_magic_x_for_offset
        y_offset = -x_offset / laser_workspace_magic_y_for_offset

        points_with_focus_compensation = []
        for point_3D in cutline_3D:
            point_3D[0] *= x_factor
            point_3D[1] *= y_factor
            point_3D[0] += x_offset
            point_3D[1] += y_offset
            points_with_focus_compensation.append(point_3D)

        return points_with_focus_compensation

    @staticmethod
    def project_on_laser_plane(qv_laser_cut_curve, projection_matrix):
        """Project 3D compensated points onto the laser plane.

        Args:
            qv_laser_cut_curve: list of [x,y,z]
            projection_matrix: np.array - np.float64

        Returns:
            cv2DPoints_projected: list of [x,y]

        """
        projected_points_2D = []
        for i in range(len(qv_laser_cut_curve)-1):
            pt1 = qv_laser_cut_curve[i]
            pt2 = qv_laser_cut_curve[i + 1]
            points_3D = np.array([[pt1[0], pt2[0]],
                                 [pt1[1], pt2[1]],
                                 [pt1[2], pt2[2]],
                                 [1, 1]], dtype=np.float64)

            points_2D = np.dot(projection_matrix, points_3D)

            # TODO: should we shift the magic numbers to to a more
            #       general place?
            x = 0
            y = 2
            width = points_2D.shape[1]
            height = 1
            w = points_2D[y:y + height, x:x + width]
            w = 1 / w

            cv_to_scale = w.copy()
            cv_to_scale = cv2.vconcat([cv_to_scale, w])
            cv_to_scale = cv2.vconcat([cv_to_scale, w])
            points_2D = cv2.multiply(points_2D, cv_to_scale)

            pt1 = [points_2D[0, 0], points_2D[1, 0]]
            pt2 = [points_2D[0, 1], points_2D[1, 1]]

            projected_points_2D.append(pt1)
            projected_points_2D.append(pt2)

        return projected_points_2D

    @staticmethod
    def match_points_to_laser_coordinate_system(points):
        """Transform points onto laser coordinates changing the coord.-axis."""
        points_in_laser_coordinate_system = []
        if len(points):
            if settings.MACHINE == 'manual_laser':
                x = np.array(points)[:, 1] * -1
                y = np.array(points)[:, 0]
            else:
                x = np.array(points)[:, 0]
                y = np.array(points)[:, 1]

            points_in_laser_coordinate_system = []
            for x_pt, y_pt in zip(x, y):
                points_in_laser_coordinate_system.append([x_pt, y_pt])

        return points_in_laser_coordinate_system

    @staticmethod
    def find_disparity(point_2D_xy, disparity_map):
        """Find the disparity for the closes point using a spiral approach.

        Args:
            point_2D_xy: [x,y]
            disparity_map: np.array - np.float64

        Returns:
            Disparity position: float64 or -1

        """
        curr_min_x = point_2D_xy[0]
        curr_max_x = point_2D_xy[0]
        min_x = point_2D_xy[0]
        max_x = point_2D_xy[0]

        curr_min_y = point_2D_xy[1]
        curr_max_y = point_2D_xy[1]
        min_y = point_2D_xy[1]
        max_y = point_2D_xy[1]

        direction = 0  # 0 - Up, 1 - Right, 2 - Down, 3 - Left
        dir_x = 1
        dir_y = 1
        found_values = 5
        rect_size = min(disparity_map.shape[1], disparity_map.shape[0]) / 20
        qv_disparities = []
        while True:
            if dir_y == 1:
                start_range_y = curr_min_y
                stop_range_y = curr_max_y + 1
                step_diff_y = 1
            else:
                start_range_y = curr_min_y
                stop_range_y = curr_max_y - 1
                step_diff_y = -1

            for i_y in np.arange(start=start_range_y,
                                 stop=stop_range_y,
                                 step=step_diff_y):
                if dir_x == 1:
                    start_range_x = curr_min_x
                    stop_range_x = curr_max_x + 1
                    step_diff_x = 1
                else:
                    start_range_x = curr_max_x,
                    stop_range_x = curr_min_x - 1,
                    step_diff_x = -1

                for i_x in np.arange(start=start_range_x,
                                     stop=stop_range_x,
                                     step=step_diff_x):
                    curr_disp = disparity_map[i_y, i_x]
                    if (curr_disp > 0):
                        qv_disparities.append(curr_disp)

                    if (len(qv_disparities) == found_values):
                        qv_disparities.sort()
                        disp = qv_disparities[int(0.6 *
                                                  len(qv_disparities))]
                        return disp

            if direction == 0:
                min_y -= 1
                curr_min_y = min_y
                curr_max_y = max_y - 1
                curr_min_x = curr_max_x = min_x
                dir_y = -1

            elif direction == 1:
                curr_min_x = min_x + 1
                max_x += 1
                curr_max_x = max_x
                curr_min_y = curr_max_y = min_y
                dir_x = 1

            elif direction == 2:
                curr_min_y = min_y + 1
                max_y += 1
                curr_max_y = max_y
                curr_min_x = curr_max_x = max_x
                dir_y = 1

            elif direction == 3:
                min_x -= 1
                curr_min_x = min_x
                curr_max_x = max_x - 1
                curr_min_y = curr_max_y = max_y
                dir_x = -1

            found_values = min(101, max(11, (2 * math.fabs(max_x - min_x)) +
                                            (2 * math.fabs(max_y - min_y))))
            direction = (direction + 1) % 4  # change direction

            if (curr_min_x >= 0
                    and curr_max_x < disparity_map.shape[1]
                    and curr_min_y >= 0
                    and curr_max_y < disparity_map.shape[0]
                    and curr_max_x - curr_min_x < rect_size
                    and curr_max_y - curr_min_y < rect_size):
                break
        return -1

    def get_camera_matrix(self):
        """Calculate the camera matrix for a global coordinate system.

        Returns:
            camera matrix == P: np.array(4,4) - np.float32

        """
        translate1 = np.identity(4, dtype=np.float32)
        translate1[:-1, -1] = self.origin_translation_vector_before
        print("===========")
        print(self.origin_euler_angles)
        print(self.origin_translation_vector_before)
        print(self.origin_translation_vector_after)

        translate2 = np.identity(4, dtype=np.float32)
        translate2[:-1, -1] = self.origin_translation_vector_after

        rotate = np.identity(4, dtype=np.float32)
        r = Rotation.from_euler('xyz', self.origin_euler_angles,
                                degrees=True).as_matrix()
        rotate[:3, :3] = r
        print("----------------------------------------")
        print(np.matmul(np.matmul(translate2, rotate), translate1))
        return np.matmul(np.matmul(translate2, rotate), translate1)

    def map_image_cut_onto_laser_plane(self, points_2D, disparity_map):
        """Map to image plane cut line onto the laser plane.

        Args:
            points_2D: list of [x,y]
            left_stereo_image: np.array - np.uint8
            right_stereo_image: np.array - np.uint8

        Returns:
            cutline_points_on_laser_plane = [x, y]

        """
        # left_stereo_image_rectified_distorted = \
        #     cv2.remap(left_stereo_image,
        #               self.rectified_map1_left_image,
        #               self.rectified_map2_left_image,
        #               cv2.INTER_LINEAR)
        #
        # right_stereo_image_rectified_distorted = \
        #     cv2.remap(right_stereo_image,
        #               self.rectified_map1_right_image,
        #               self.rectified_map2_right_image,
        #               cv2.INTER_LINEAR)
        #
        # disparity_map = CutLineGeneration.compute_disparity_map(
        #     left_stereo_image_rectified_distorted,
        #     right_stereo_image_rectified_distorted)

        points_3D = CutLineGeneration.calculate_3D_Cut_curve(
            points_2D,
            disparity_map,
            self.q_matrix)

        focalplane_adjustment = \
            CutLineGeneration.get_median_focal_plane_adjustment(self,
                                                                points_3D)
        print("focal_adjust", focalplane_adjustment)
        print(np.unique(disparity_map))

        compensated_points_3D = CutLineGeneration.compensate_focus(
            points_3D,
            focalplane_adjustment)

        unmatched_cutline_points_on_laser_plane = \
            CutLineGeneration.project_on_laser_plane(
                compensated_points_3D,
                self.projection_matrix_to_laser)

        cutline_points_on_laser_plane = \
            CutLineGeneration.match_points_to_laser_coordinate_system(
                unmatched_cutline_points_on_laser_plane)

        return cutline_points_on_laser_plane, int(focalplane_adjustment)
