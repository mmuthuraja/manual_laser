var canvas_data = {
    "image_id": "",
    "cut_lines": []
}
var curX, curY, prevX, prevY;
var hold = false;
var one_stroke;
var fill_value = true;
var stroke_value = false;
var line_id = 0;
var canvas;
var ctx;
var img = new Image(0, 0);
var img_src;
var img_data;
var cutBtn;
var currImgId;
const csrftoken = getCookie('csrftoken');
var canvas_resp_width;
var canvas_resp_height;
const image_ratio = 4 / 3;
var viewport_width;
var viewport_height;

var max_scale;
var zoom_factor;
const canvas_grid_col = 12/5; // Bootstrap total_columns/canvas_column_number
const magic_viewport_height_tolerence = 100;

// For page refresh
var myEvent = window.attachEvent || window.addEventListener;
var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';

myEvent(chkevent, function(e) {
    sessionStorage.setItem("name", $('#inputOperatorName').val());
    sessionStorage.setItem("species", $('#inputPlantSpecies').val());
    sessionStorage.setItem("cloneid", $('#inputCloneId').val());
    sessionStorage.setItem("customer", $('#inputCustomerInfo').val());
    sessionStorage.setItem("laser_power", $('#LaserPower').val());
});

function set_canvas_properties() {
    viewport_width = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    viewport_height = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
    canvas_resp_width = viewport_width / canvas_grid_col;
    canvas_resp_height = viewport_height - magic_viewport_height_tolerence;
    // canvas_resp_height = (viewport_width / canvas_grid_col) * image_ratio;
    canvas = document.getElementById("cutLineCanvas");


    canvas.width = canvas_resp_width;
    canvas.height = canvas_resp_height;
    document.getElementById("scroll").style.maxWidth = canvas_resp_width + "px";
    document.getElementById("scroll").style.maxHeight = canvas_resp_height + "px";
}

function set_zoom_parameters() {
    max_scale = 4;
    zoom_factor = 0.1;
}

function set_cut_button_properties() {
    cutBtn = document.getElementById("cutimage");
    cutBtn.disabled = true;
}

function set_meta_data(){
    var name = sessionStorage.getItem("name");
    var species = sessionStorage.getItem("species");
    var cloneid = sessionStorage.getItem("cloneid");
    var customer = sessionStorage.getItem("customer");
    var laser_power = sessionStorage.getItem("laser_power");
    if (name !== null) $('#inputOperatorName').val(name);
    if (species !== null) $('#inputPlantSpecies').val(species);
    if (cloneid !== null) $('#inputCloneId').val(cloneid);
    if (customer !== null) $('#inputCustomerInfo').val(customer);
    if ((laser_power !== null) && (laser_power !== 50)){
        $('#LaserPower').val(laser_power);
        document.getElementById('LasertextInput').value=laser_power;
    }
}

function manual_laser_onload() {
    set_meta_data();
    $('#LasertextInput').attr('readonly', true);
    set_canvas_properties();
    set_zoom_parameters();
    set_cut_button_properties();
    ctx = canvas.getContext("2d");
    document.getElementById('cutLineCanvas').addEventListener('change', readSingleFile, false);


    var scale = 1;
    var cutline_metadata = {
        "line_id": line_id,
        "points": []
    }
    one_stroke = true;

    canvas.onmousedown = function(e) {
        if (one_stroke) {
            hold = true;
            var pos = getMousePosition(canvas, e);
            ctx.fillStyle = 'blue';
            ctx.strokeStyle = 'red';
            ctx.lineWidth = 3;
            ctx.beginPath();
            ctx.moveTo(pos.x, pos.y);
        }
    };

    canvas.onmousemove = function(e) {
        if (hold && one_stroke) {
            var pos = getMousePosition(canvas, e);
            curX = pos.x;
            curY = pos.y;
            draw();
        }
    };

    canvas.onmouseup = function(e) {
        hold = false;
        one_stroke = false;
        line_id = line_id + 1;
        canvas_data.cut_lines.push(cutline_metadata);
        if (canvas_data.cut_lines.length > 0) {
            cutBtn.disabled = false;
        }
    };

    canvas.onmouseout = function(e) {
        hold = false;
    };

    canvas.onwheel = function(e) {
        if (img.width !== 0 && img.height !== 0 && canvas_data.cut_lines == 0) {
            e.preventDefault();
            var delta = event.deltaY < 0 ? 1 : -1;
            delta = Math.max(-1, Math.min(1, delta))
            scale += delta * zoom_factor * scale
            scale = Math.max(1, Math.min(max_scale, scale))
            zoom_image();
        }
    }

    function draw() {
        ctx.lineTo(curX, curY);
        ctx.stroke();
        scale_x_position = (img.width / canvas.width) * curX;
        scale_y_position = (img.height / canvas.height) * curY;
        cutline_metadata.points.push([parseInt(scale_x_position), parseInt(scale_y_position)])
    }

    function zoom_image() {
        if (scale > 1) {
            document.getElementById("scroll").style.overflow = "auto";
        } else {
            document.getElementById("scroll").style.overflow = "hidden";
        }
        canvas.width = canvas_resp_width * scale;
        canvas.height = canvas_resp_height * scale;
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
    }
}

function readSingleFile(e) {
    var file = e.target.files[0];
    if (!file) {
        return;
    }
    var reader = new FileReader();
    reader.onload = function(e) {
        var contents = e.target.result;
        img = new Image;
        img.src = reader.result;
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
        }
    }
    reader.readAsDataURL(file);
}

function getMousePosition(canvas, evt) {
    var rect = canvas.getBoundingClientRect(), // abs. size of element
        scaleX = canvas.width / rect.width, // relationship bitmap vs. element for X
        scaleY = canvas.height / rect.height; // relationship bitmap vs. element for Y

    return {
        x: (evt.clientX - rect.left) * scaleX, // scale mouse coordinates after they have
        y: (evt.clientY - rect.top) * scaleY // been adjusted to be relative to element
    }
}

function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0, img.width, img.height,
        0, 0, canvas.width, canvas.height);
    canvas_data.cut_lines = [];
}

function reset_image() {
    if (img.width !== 0 && img.height !== 0){
        sessionStorage.setItem("laser_power", $('#LaserPower').val());
        manual_laser_onload();
        clearCanvas();
        cutBtn.disabled = true;
        line_id = 0;
        document.getElementById("scroll").style.overflow = "hidden";
    }
}

function set_image_id(imgid){
    currImgId = imgid;
}

function set_image_src(selector) {
    $.ajax({
        url: "/manual-laser/get-image",
        type: 'GET',
        cache: false,
        success: function (data) {
            $(selector).attr("src", "data:image/png;base64," + data['image_data']);
            set_image_id(data['image_id']);
        },
        error: function () {
            alert("Unable to fetch Image");
        }
    });
}

function get_image() {
    reset_image();
    img = new Image;
    set_image_src(img)
    img.onload = function() {
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
        img_data = canvas.toDataURL();
    };
    currImgId = "";
}

function cut_image() {
    var operator_info = document.getElementById("inputOperatorName").value;
    var species_info = document.getElementById("inputPlantSpecies").value;

    if (img.width == 0 && img.height == 0) {
        window.alert("No Image Loaded");
        return;
    } else if (operator_info.length == 0 || operator_info == "") {
        window.alert("Enter operator Name");
        return;
    } else if (species_info.length == 0 || species_info == "") {
        window.alert("Enter Species Name");
        return;
    }
    console.log("canvas_data.cut_lines:", canvas_data.cut_lines)
    var raw_msg = {
        "imageId": currImgId,
        "imageData": img_data,
        "cutLine": canvas_data.cut_lines,
        "operatorName": document.getElementById('inputOperatorName').value,
        "plantSpecies": document.getElementById('inputPlantSpecies').value,
        "cloneId": document.getElementById('inputCloneId').value,
        "customerInfo": document.getElementById('inputCustomerInfo').value,
        "powerValue": document.getElementById('LaserPower').value
    };
    var string_msg = JSON.stringify(raw_msg);
    $.ajax({
        url: "/manual-laser/laser-cut",
        method: "POST",
        headers: {
            'X-CSRFToken': csrftoken
        },
        data: string_msg,
        dataType: 'json',
    }).done(function(response) {
        console.log(response.id + " " + response.name);
    }).fail(function(error) {
        console.log(error);
    });
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function updateTextInput(val) {
          document.getElementById('LasertextInput').value=val;
        }
