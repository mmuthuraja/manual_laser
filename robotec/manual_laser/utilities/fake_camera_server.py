"""Fake Camera Server."""
import json
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import base64


class RequestHandler(BaseHTTPRequestHandler):
    """Class HTTP RequestHandler."""

    def do_GET(self):
        """Get request."""
        self.send_response(200)
        self.end_headers()

        root_folder = os.path.abspath(".").replace("utilities", "")
        left_image_path = os.path.join(root_folder,
                                       "test_images/img/Left_Raw.png")
        right_image_path = os.path.join(root_folder,
                                        "test_images/img/Right_Raw.png")

        with open(left_image_path, "rb") as image_file:
            left_image_data = base64.b64encode(image_file.read())

        with open(right_image_path, "rb") as image_file:
            right_image_data = base64.b64encode(image_file.read())

        data = {"left_image": {"widht": "12345",
                               "height": "123456",
                               "data": str(left_image_data.decode())},
                "right_image": {"widht": "12345",
                                "height": "123456",
                                "data": str(right_image_data.decode())}
                }
        self.wfile.write(json.dumps(data).encode())

        return

    def do_POST(self):
        """Post request."""
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body)
        self.send_response(200)
        self.end_headers()
        self.wfile.write(json.dumps({
            'body': data,
            'job_status': "accepted",
        }).encode())
        return


def start_test_http_server(server):
    """Start a test server."""
    thread = threading.Thread(target=server.serve_forever)
    thread.daemon = True
    thread.start()


def stop_test_http_server(server):
    """Stop http test sever."""
    server.shutdown()


server = HTTPServer(("localhost", 7777), RequestHandler)
server.serve_forever()
