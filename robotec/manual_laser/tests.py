"""Create Test Here."""

from django.test import TestCase
from django.urls import reverse
from .laser_calculations import CutLineGeneration
import json
import numpy as np
import cv2
import os
from http.server import BaseHTTPRequestHandler, HTTPServer
import threading
from django.conf import settings


import base64
from .models import StereoImagesModel
import uuid


class ManualLaserRestApiTests(TestCase):
    """Test class handling responses only."""

    def __init__(self, *args, **kwargs):
        """Manual Laser Tests Init."""
        super(ManualLaserRestApiTests, self).__init__(*args, **kwargs)
        self.image_coordinates = [[0, 0], [0, 0]]
        self.laser_coordinates = [[133.33333333333334, -100.0],
                                  [133.33333333333334, -100.0]]

    def test_manual_laser_status_code(self):
        """manual_laser test for status code."""
        response = self.client.get(reverse('manual_laser'))
        self.assertEqual(response.status_code, 200)

    def test_manual_laser_html(self):
        """Manual_laser test for html."""
        response = self.client.get(reverse('manual_laser'))
        assert (b'<html>' and b'</html>') in response.content

    def test_manual_laser_post_laser_cut_with_dummy_json(self):
        """Test POST:/manual-laser/laser-cut request by passing dummy JSON."""
        settings.LASER_GATEWAY_IP = "localhost"
        laser_server = HTTPServer((settings.LASER_GATEWAY_IP,
                                   settings.LASER_GATEWAY_PORT),
                                  RequestHandler)

        json_dict = {
                "imageId": "12345",
                "imageData": "12345",
                "cutLine": [{"line_id": 1, "points": [[0, 0], [0, 0]]}],
                "operatorName": "Operator Nr1",
                "plantSpecies": "coca",
                "cloneId": "Patient 0",
                "customerInfo": "cartel",
                "powerValue": "9000"
        }

        start_test_http_server(laser_server)
        response = self.client.generic('POST', '/manual-laser/laser-cut',
                                       json.dumps(json_dict))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(b'Finished', response.content)
        stop_test_http_server(laser_server)

    def test_manual_laser_post_laser_cut_with_real_image_id(self):
        """Test POST:/manual-laser/laser-cut request with real image_id."""
        settings.LASER_GATEWAY_IP = "localhost"
        laser_server = HTTPServer((settings.LASER_GATEWAY_IP,
                                   settings.LASER_GATEWAY_PORT),
                                  RequestHandler)

        root_folder = os.path.abspath(".").split("/")[-1]
        if "home" == root_folder or "robotec-application" == root_folder:
            data_path = os.path.abspath("robotec")
        else:
            data_path = os.path.abspath(".")

        left_image_path = \
            os.path.join(data_path,
                         "manual_laser/test_images/img/Left_Raw.png")
        right_image_path = \
            os.path.join(data_path,
                         "manual_laser/test_images/img/Right_Raw.png")

        with open(left_image_path, "rb") as image_file:
            left_image_data = base64.b64encode(image_file.read())
        with open(right_image_path, "rb") as image_file:
            right_image_data = base64.b64encode(image_file.read())

        stereoimagemodel = StereoImagesModel()
        stereoimagemodel.left_image = str(left_image_data.decode())
        stereoimagemodel.right_image = str(right_image_data.decode())
        stereoimagemodel.stereo_id = str(uuid.uuid4())
        stereoimagemodel.save()

        cutline = [[987, 789],
                   [1000, 785],
                   [1021, 777],
                   [1050, 762],
                   [1069, 756],
                   [1082, 750]]

        json_dict = {
            "imageId": stereoimagemodel.stereo_id,
            "imageData": "12345",
            "cutLine": [{"line_id": 20, "points": cutline}],
            "operatorName": "Operator Nr2",
            "plantSpecies": "hemp",
            "cloneId": "Patient 1",
            "customerInfo": "cartel",
            "powerValue": "3000"
        }

        start_test_http_server(laser_server)
        response = self.client.generic('POST', '/manual-laser/laser-cut',
                                       json.dumps(json_dict))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(b'Finished', response.content)
        stop_test_http_server(laser_server)

    def test_manual_laser_post_laser_cut_bad_request(self):
        """Test POST:/manual-laser/laser-cut request passing invalid json."""
        json_dict = {
                "imageData": "12345",
                "cutLine": "12345",
        }
        response = self.client.generic('POST', '/manual-laser/laser-cut',
                                       json.dumps(json_dict))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(b'BAD REQUEST', response.content)

    def test_load_calibration_matrix(self):
        """Test case for loading correct number of calibration matrices."""
        calibration_dict = CutLineGeneration.read_calibration_matrices()
        expected_matrices = ['LeftUpperIntrinsic', 'LeftLowerIntrinsic',
                             'LeftUpperDistortion', 'LeftLowerDistortion',
                             'LeftUpperRotation', 'LeftLowerRotation',
                             'LeftUpperProjection', 'LeftLowerProjection',
                             'cvInputQ', 'LaserProjMatrix',
                             'OriginTranslationVectorBefore',
                             'OriginTranslationVectorAfter',
                             'OriginEulerAngles']
        matrices_loaded = True
        for key in calibration_dict.keys():
            if not (key in expected_matrices):
                matrices_loaded = False
                break
        self.assertEqual(matrices_loaded, True)

    def test_get_image(self):
        """Test case for GET:get-image."""
        settings.CAMERA_GATEWAY_IP = "localhost"
        server = HTTPServer((settings.CAMERA_GATEWAY_IP,
                             settings.CAMERA_GATEWAY_PORT), RequestHandler)
        start_test_http_server(server)
        response = self.client.generic('GET', '/manual-laser/get-image')
        self.assertEqual(response.status_code, 200)
        assert type(response.content).__name__ == "bytes"
        stop_test_http_server(server)


class LaserCalculationsTests(TestCase):
    """Test verifying the functionality of the laser_calculations.py."""

    def test_compensated_cutline_via_predefined_focal_plane_factor(self):
        """Test whether the uncompensated cut line is compensated correctly."""
        uncompensated_cutline = np.array([[0.0173625, -0.0368129, 0.334437],
                                          [-0.0401086, 0.0645893, 0.291366],
                                          [-0.0531705, 0.0773121, 0.28733]],
                                         dtype=np.float32)
        focalplane_adjustment = -19.2433

        calculated_compensated_cutline = CutLineGeneration.compensate_focus(
            uncompensated_cutline,
            focalplane_adjustment)

        given_compensated_cutline = np.array(
            [[0.02018893137574196, -0.04004194959998131, 0.3344370126724243],
             [-0.0402681902050972, 0.0676044449210167, 0.29136601090431213],
             [-0.05400874465703964, 0.08111069351434708, 0.2873300015926361]],
            dtype=np.float32)

        self.assertTrue((calculated_compensated_cutline ==
                         given_compensated_cutline).all())

    def test_compensated_cutline_via_zero_focal_plane_factor(self):
        """Compensate via zero adjustment factor."""
        uncompensated_cutline = np.array([[0.0173625, -0.0368129, 0.334437],
                                          [-0.0401086, 0.0645893, 0.291366],
                                          [-0.0531705, 0.0773121, 0.28733]],
                                         dtype=np.float32)

        focalplane_adjustment = 0

        calculated_compensated_cutline = CutLineGeneration.compensate_focus(
            uncompensated_cutline,
            focalplane_adjustment)

        given_compensated_cutline = np.array(
            [[0.0173625, -0.0368129, 0.334437],
             [-0.0401086, 0.0645893, 0.291366],
             [-0.0531705, 0.0773121, 0.28733]],
            dtype=np.float32)

        self.assertTrue((calculated_compensated_cutline ==
                         given_compensated_cutline).all())

    def test_if_focal_plane_adjustment_gets_correctly_calculated(self):
        """Test of focal_plane_adjustment calculation."""
        points_3D = np.array([[0.0173625, -0.0368129, 0.334437],
                              [-0.0401086, 0.0645893, 0.291366],
                              [-0.0531705, 0.0773121, 0.28733]],
                             dtype=np.float32)

        obj = CutLineGeneration()
        calculated_focalPlane_adjustment = \
            obj.get_median_focal_plane_adjustment(points_3D)

        correct_focalPlane_adjustment = -19.2433

        self.assertEqual(correct_focalPlane_adjustment,
                         np.round(calculated_focalPlane_adjustment,
                                  decimals=4))

    def test_project_on_laser_plane(self):
        """Test the projection of cut line on the world laser plane."""
        points_3D_compensated = np.array(
            [[0.0201531, -0.0400012, 0.334437],
             [-0.0402662, 0.0675663, 0.291366],
             [-0.0539981, 0.0810627, 0.28733]],
            dtype=np.float32)

        given_cutline_points_on_laser_plane = np.array(
            [[-67.6014, -53.6726],
             [39.9423, 7.3635],
             [39.9423, 7.3635],
             [52.2946, 20.5155]],
            dtype=np.float32)

        obj = CutLineGeneration()
        calculated_cutline_points_on_laser_plane = \
            CutLineGeneration.project_on_laser_plane(
                points_3D_compensated,
                obj.projection_matrix_to_laser)

        # Rounding is important as well as the proper type
        round_factor = 3
        x = np.round(given_cutline_points_on_laser_plane,
                     decimals=round_factor).astype(np.float32)
        y = np.round(calculated_cutline_points_on_laser_plane,
                     decimals=round_factor).astype(np.float32)

        epsilon = 0.001
        difference = np.round(np.abs(x - y), decimals=round_factor)
        difference = np.less_equal(difference, epsilon)

        self.assertTrue(difference.all())

    def test_calculate_3D_Cut_curve_from_image_2D_points(self):
        """Tests calculate_3D_Cut_curve using reference values."""
        calibration_dict = CutLineGeneration.read_calibration_matrices()

        left_rectified_image = cv2.imread(Utility.get_path_to_file(
            "manual_laser/test_images/img/LeftRect.png"))
        right_rectified_image = cv2.imread(Utility.get_path_to_file(
            "manual_laser/test_images/img/RightRect.png"))

        cutcurve_2D_XY = np.array([[800, 1000], [1200, 300], [1300, 200]])

        disparitymap = CutLineGeneration.compute_disparity_map(
            left_rectified_image,
            right_rectified_image)

        cvInputQ = np.array(calibration_dict['cvInputQ'])

        calculated_cutline3d = CutLineGeneration.calculate_3D_Cut_curve(
                                        cutcurve_2D_XY,
                                        disparitymap,
                                        cvInputQ)

        correct_cutline3d_matrix = np.array([[0.0173625, -0.0368129, 0.334437],
                                             [-0.0401086, 0.0645893, 0.291366],
                                             [-0.0531705, 0.0773121, 0.28733]],
                                            dtype=np.float32)

        dec_factor = 3
        x = np.round(np.array(calculated_cutline3d, dtype=np.float32),
                     decimals=dec_factor)
        y = np.round(correct_cutline3d_matrix, decimals=dec_factor)

        epsilon = 0.001
        difference = np.round(np.abs(x-y), decimals=dec_factor)
        difference = np.less_equal(difference, epsilon)

        self.assertTrue(difference.all())

    def test_complete_laser_calculation_pipeline_for_points(self):
        """Test generated_3D_points functionality."""
        points_2D = np.array([[800, 1000], [1200, 300], [1300, 200]])

        left_stereo_image = cv2.imread(Utility.get_path_to_file(
            "manual_laser/test_images/img/Left_Raw.png"))
        right_stereo_image = cv2.imread(Utility.get_path_to_file(
            "manual_laser/test_images/img/Right_Raw.png"))

        correct_cutline_points_on_laser_plane = np.array([[-67.6014, -53.6726],
                                                          [39.9423, 7.3635],
                                                          [39.9423, 7.3635],
                                                          [52.2946, 20.5155]],
                                                         dtype=np.float32)

        correct_cutline_points_on_laser_plane = \
            CutLineGeneration().match_points_to_laser_coordinate_system(
                correct_cutline_points_on_laser_plane)

        obj = CutLineGeneration()
        calculated_cutline_points_on_laser_plane, _ = \
            obj.map_image_cut_onto_laser_plane(points_2D,
                                               left_stereo_image,
                                               right_stereo_image)

        dec_factor = 3
        x = np.round(np.array(correct_cutline_points_on_laser_plane,
                              dtype=np.float32),
                     decimals=dec_factor)
        y = np.round(calculated_cutline_points_on_laser_plane,
                     decimals=dec_factor)

        epsilon_mm = 0.5
        difference = np.round(np.abs(x-y), decimals=dec_factor)
        difference = np.less_equal(difference, epsilon_mm)

        self.assertTrue(difference.all())

    def test_complete_laser_calculation_pipeline_for_focus_value(self):
        """Test generated_3D_points functionality for laser focus adjust."""
        points_2D = np.array([[800, 1000], [1200, 300], [1300, 200]])

        left_stereo_image = cv2.imread(Utility.get_path_to_file(
            "manual_laser/test_images/img/Left_Raw.png"))
        right_stereo_image = cv2.imread(Utility.get_path_to_file(
            "manual_laser/test_images/img/Right_Raw.png"))

        obj = CutLineGeneration()
        _, calculated_focus_adjust = \
            obj.map_image_cut_onto_laser_plane(points_2D,
                                               left_stereo_image,
                                               right_stereo_image)

        correct_focalPlane_adjustment = -19

        self.assertEqual(correct_focalPlane_adjustment,
                         calculated_focus_adjust)

    def test_match_points_to_laser_coordinate_system_on_empty_input(self):
        """Tests match_points_to_laser_coordinate_system on empty set."""
        invalid_input = []
        points = CutLineGeneration().\
            match_points_to_laser_coordinate_system(invalid_input)

        desired_points = []
        self.assertTrue(len(desired_points) == len(points))




# ###########################  Utilities  #####################################
class Utility:
    """Class loading camera_matrices via absolut path."""

    @staticmethod
    def get_path_to_file(file_name):
        """Get Path to a file."""
        root_folder = os.path.abspath(".").split("/")[-1]
        if "home" == root_folder or "robotec-application" == root_folder:
            data_path = os.path.abspath("robotec")
        else:
            data_path = os.path.abspath(".")
        return os.path.join(data_path, file_name)


# Fake camera and laser gateway
class RequestHandler(BaseHTTPRequestHandler):
    """Class HTTP RequestHandler."""

    def do_GET(self):
        """Get request."""
        self.send_response(200)
        self.end_headers()
        data = {
                "left_image": {"widht": "12345",
                               "height": "123456",
                               "data": "iVBORw0KGgoAAAANSUhEUgAAAAMAAAAD"
                               "CAAAAABzQ+pjAAAAFElEQVQIHWNkYGBgZGBg"
                               "YGRgYAAAACQABOVZymwAAAAASUVORK5CYII="},
                "right_image": {"widht": "12345",
                                "height": "123456",
                                "data": "iVBORw0KGgoAAAANSUhEUgAAAAMAAAAD"
                                "CAAAAABzQ+pjAAAAFElEQVQIHWNkYGBgZGBg"
                                "YGRgYAAAACQABOVZymwAAAAASUVORK5CYII="}
        }
        self.wfile.write(json.dumps(data).encode())

        return

    def do_POST(self):
        """Post request."""
        content_len = int(self.headers.get('content-length'))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body)
        self.send_response(200)
        self.end_headers()
        self.wfile.write(json.dumps({
            'body': data,
            'job_status': "accepted",
        }).encode())
        return


def start_test_http_server(server):
    """Start a test server."""
    thread = threading.Thread(target=server.serve_forever)
    thread.daemon = True
    thread.start()


def stop_test_http_server(server):
    """Stop http test sever."""
    server.shutdown()
