"""Views."""

import json
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from .models import StereoImagesModel
from .laser_calculations import CutLineGeneration
from .http import get_stereo_images, post_data_to_laser_gateway, \
    get_images_and_maps_from_camera_daemon
from .utility_functions import save_stereo_images, \
    save_cutline_and_metadata, \
    rotate_base64_image_string, \
    load_image_from_test_images_folder, \
    obtain_stereo_images, \
    rotate_by_ninety_degree, \
    un_mirror_image, \
    is_valid_data, \
    is_valid_image_id

from django.conf import settings



def manual_laser(request):
    """Index."""
    return render(request, 'manual_laser/manual_laser.html')


# def get_image(request):
#     """get_image request."""
#     if request.method == 'GET':
#         print("get_image is called")
#         stereo_image_dict = get_stereo_images()
#         generated_stereo_id = save_stereo_images(stereo_image_dict)
#         left_image_rotated_clockwise_90 = rotate_base64_image_string(
#             stereo_image_dict['left_image'])
#
#         data = {"image_data": str(left_image_rotated_clockwise_90),
#                 "image_id": generated_stereo_id}
#         response = JsonResponse(data)
#         response['Cache-Control'] = 'no-cache,max-age=0'
#
#         return response


def get_image(request):
    """get_image request."""
    if request.method == 'GET':
        print("get_image is called")
        generated_stereo_id = "random"
        image_dict = get_images_and_maps_from_camera_daemon(
            settings.CAMERA_GATEWAY_IP, settings.CAMERA_GATEWAY_PORT)
        stereo_image_dict = dict()
        stereo_image_dict['left_image'] = image_dict['encoded_img']
        stereo_image_dict['right_image'] = image_dict['encoded_disparity']

        generated_stereo_id = save_stereo_images(stereo_image_dict)
        left_image_rotated_clockwise_90 = rotate_base64_image_string(
            image_dict['encoded_img'])

        data = {"image_data": str(left_image_rotated_clockwise_90),
                "image_id": generated_stereo_id}
        response = JsonResponse(data)
        response['Cache-Control'] = 'no-cache,max-age=0'

        return response


def laser_cut(request):
    """laser_cut request."""
    if request.method == 'POST':
        json_data = json.loads(request.body)
        queryset = StereoImagesModel.objects.all()

        if is_valid_data(json_data):
            save_cutline_and_metadata(json_data)

            if is_valid_image_id(json_data['imageId'], queryset):
                left_stereo_image, disparity_map = \
                    obtain_stereo_images(json_data['imageId'])
                left_stereo_image_un_mirrored = \
                    un_mirror_image(left_stereo_image)
                # right_stereo_image_un_mirrored = \
                #     un_mirror_image(right_stereo_image)
            # else:
            #     left_stereo_image_un_mirrored, right_stereo_image_un_mirrored \
            #         = load_image_from_test_images_folder()

            # New laser transformations
            points_2d = \
                rotate_by_ninety_degree(
                    json_data['cutLine'][0]['points'],
                    left_stereo_image_un_mirrored.shape[0]-1)

            cutline_points_on_laser_plane, focus_adjust = \
                CutLineGeneration().map_image_cut_onto_laser_plane(
                    points_2d,
                    disparity_map)
            poly_line_data = dict()
            poly_line_data["line_id"] = 1;
            poly_line_data['points'] = \
                cutline_points_on_laser_plane
            poly_line_data['focus'] = focus_adjust
            poly_line_data['power'] = 100
            poly_line_data["velocity"] = 10
            final_json = {
                'cut': poly_line_data
            }

            print("Transformed Points:",
                  cutline_points_on_laser_plane)
            print("final_json", final_json)

            return post_data_to_laser_gateway(final_json)
        else:
            return HttpResponse("BAD REQUEST")
